let mix = require('laravel-mix');




mix.autoload({
  jquery: ['$', 'jQuery', 'window.jQuery'],
  tether: ['Tether', 'window.Tether'],
  moment: 'moment'
});


mix.js([
	'resources/assets/js/bootstrap.min.js',
  'resources/assets/js/app.js'],

    'public/js');

mix.sass('resources/assets/sass/app.scss', 'public/css');


mix.styles([
  // Style Alumno

    'resources/assets/css/custom-style_alumno.css',
    // 'resources/assets/css/font-awesome.css',

  // Style Profesor
    


  // Style Moderador
  


  // Style Admin
   
   // 'resources/assets/css/custom-style_cpanel.css',

], 'public/css/all.css');
