@extends('layouts.app')

@section('solicitarclase')


<!-- <solicitarclase></solicitarclase> -->

<div class="container">
  <br>
  <fieldset class="scheduler-border mt-md-5">
    <legend class="scheduler-border">Solicita tu clase</legend>
    <form action="">
    <div class="form-group row">
      <div class="col-md-4">
          <label for="sel1">Curso:</label>
          <select class="form-control" id="sel1">
            <option>Matemática</option>
            <option>Lenguaje</option>
            <option>Ingles</option>
            <option>Biologia</option>
          </select>
      </div>
      <div class="col-md-4">
          <label for="sel1">Nivel:</label>
          <select class="form-control" id="sel1">
            <option>1° Secundaria</option>
            <option>2° Secundaria</option>
            <option>3° Secundaria</option>
            <option>4° Secundaria</option>
            <option>5° Secundaria</option>
          </select>
      </div>
      <div class="col-md-4">
          <label for="sel1"></label>
          <input type="text" class="form-control" id="" placeholder="Centro Educativo">
        </div>
    </div>
    <div class="form-group row">
      <!-- 2 row -->
      <div class="col-md-4">
          <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
          <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" />
          <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
          </span>
        </div>
      </div>
      <div class="col-md-4">
        <label for="sel1">Adjuntar Tarea:</label>
      </div>
      <div class="col-md-4">
          <div class="form-group">
            <!-- <label for="comment">Comment:</label> -->
            <textarea class="form-control" rows="5" id="comment" placeholder="Personaliza tu clase(Como podemos ayudarte)"></textarea>
          </div>
      </div>
      <div class="col-md-12 text-right">
        <div class="form-group">
          <button type="button" class="btn btn-primary btn-md">Pedir</button>
        </div>
      </div>
    </div>
    </form>
  </fieldset>
</div>

      <!-- Mis pedidos -->

  <div class="container">
    <fieldset class="scheduler-border">
      <legend class="scheduler-border">Mis Pedidos</legend>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-hover">
        <thead>
          <tr>
            <th>Curso / Nivel</th>
            <th>Centro Educativo</th>
            <th>Fecha - Hora</th>
            <th>Adjunto</th>
            <th>Profesor Asignado</th>
            <th>Lugar</th>
            <th>Estado</th>
            <th>Precio</th>
            <th>Pagado</th>
            <th>Encuesta</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Matemática</td>
            <td>Sophianum</td>
            <td>01/10/17</td>
            <td></td>
            <td>Jirafales</td>
            <td>Domicilio</td>
            <td>Espera</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Ingles</td>
            <td>UPC</td>
            <td>29/09/17</td>
            <td></td>
            <td>Adolfo</td>
            <td>Domicilio</td>
            <td>Espera</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Matemática</td>
            <td>Sophianum</td>
            <td>26/09/17</td>
            <td class="text-center">
              <a href="#"><span class="glyphicon glyphicon-folder-open"></span></a>
            </td>
            <td>Jirafales</td>
            <td>Domicilio</td>
            <td class="bg-success">Atendido</td>
            <td>S/.20</td>
            <td class="text-center">
              <a href="#"><span class="fa fa-check"></span></a>
            </td>
            <td class="text-center">
              <a href="#"><span class="fa fa-list-alt"></span></a>
            </td>
          </tr>
        </tbody>
      </table>
        </div>
      </div>
      <div class="row">
        <!-- 2 row -->
        
      </div>
    </fieldset>
  </div>
@endsection

@section('scripts')
<script>
  $(document).ready(function(){

    alert(1);
  });
</script>

@endsection