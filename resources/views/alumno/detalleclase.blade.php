@extends('layouts.app')

@section('detalleclase')
 <div class="container">

      <!-- Mis pedidos -->
        <br>
        <fieldset class="scheduler-border mt-md-5">
          <legend class="scheduler-border">Mis Pedidos</legend>
            <div class="col-md-12">
            	<table class="table">
  				    <thead class="thead-inverse">
  				      <tr>
  				        <th>Curso / Nivel</th>
  				        <th>Centro Educativo</th>
  				        <th>Fecha - Hora</th>
  				        <th>Lugar</th>
  				        <th>Estado</th>
  				        <th>Precio</th>
  				      </tr>
  				    </thead>
  				    <tbody>
  				      <tr>
  				        <td>Matemática</td>
  				        <td>Sophianum</td>
  				        <td>01/10/17</td>
  				        <td>Domicilio</td>
  				        <td>Atendido</td>
  				        <td>S./20</td>
  				      </tr>
  				    </tbody>
  				  </table>
        </div>
      </fieldset>
  </div>


  <div class="container">

      <div class="">
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Datos del Profesor</legend>

          <div class="row middle-xs">
              <div class="col-md-10 text-center">
                  <div class="box">
                      <h2 class="text-primary"><strong>Prof. Jirafales</strong></h2>
                  </div>
              </div>
              <div class="col-md-2 text-center">
                  <div class="box">
                       <img src="images/male.png" alt="" class="img-responsive center-block" style="width: 50%;">
                      <div style="padding-top: 20px;padding-bottom: 20px;">
                        <a href="#" class="btn btn-primary">Preguntar</a>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-8 col-md-offset-1">
        
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#p" role="tab">Perfil</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#exp">Experiencia</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#a_c">Archivos Compartidos</a>
                </li>
              </ul>



              <div class="tab-content">
                <div id="p" class="tab-pane active" role="tabpanel">
                <br>
                  <h6>Profesor titulado y con amplia experiencia en Euskera, Lengua, Historia e Inglés.</h6>
                  <p class="text-left">
                    Enseñanza cercana, implicada y continua, para una mejora progresiva del alumnado.
                    Métodos actuales, tanto intensivos como constantes. Clases intensas pero adaptables según las necesidades, con especialidad en Lenguas como Euskera e Inglés, así como en materias tales como Historia, Lengua y Literatura, o Filosofía.
                  </p>
                </div>
                <div id="exp" class="tab-pane" role="tabpanel">
                <br>
                  <p>Experiencia previa en impartir clases particulares a alumnos, individualmente, así como amplia experiencia en clases particulares en academia, tanto individual como por grupos.
Numerosos alumnos de todo tipo de edades, procedencias y estudios, con buenos resultados y notoria mejoría en la mayoría de los casos.</p>
                </div>
                <div id="a_c" class="tab-pane" role="tabpanel">
                  <br>
                  <div class="col-md-12">
                      <span class="text-danger">No hay archivos!!!!</span>
                  </div>
                </div>
              </div>
           </div>

          </div>
        </fieldset>
      </div>
    </div>
@endsection