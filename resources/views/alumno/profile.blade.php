	@extends('layouts.app')

	@section('profile')
	<div class="container pt-md-5">
	   <div class="row">
	      <div class="col-md-12">
	         <div class="pt-md-4">
	            <!-- Nav tabs -->
	            <!-- <ul class="nav nav-tabs" role="tablist">
	               <li style="padding: 10px!important;" role="presentation" class="active"><a href="#perfil" aria-controls="perfil" role="tab" data-toggle="tab">Perfil</a></li>
	               <li style="padding: 10px!important;" role="presentation"><a href="#act_contra" aria-controls="act_contra" role="tab" data-toggle="tab">Actualizar Contraseña</a></li>
	            </ul> -->
	            <ul class="nav nav-tabs" role="tablist">
				  <li class="nav-item">
				    <a class="nav-link active" href="#perfil" role="tab" data-toggle="tab">Perfil</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" href="#act_contra" role="tab" data-toggle="tab">Actualizar Contraseña</a>
				  </li>
				</ul>
	            <!-- Tab panes -->
	            <div class="tab-content">
	               <div role="tabpanel" class="tab-pane active" id="perfil">
	                  <div class="col-md-12">
	                  <!--    <h2>Mi Perfil</h2> -->
	                  </div>
	                  <div class="" data-example-id="">
						<div class="form-group row">
						  <div class="col-md-4 pt-md-5">
						    <form enctype="multipart/form-data" action="/perfil" method="POST">
	                           <div class="form-group  mb-md-4">
	                              <img src="{{Auth::user()->avatar }}" style="width:50%; height:auto; float:left; border-radius:50%; margin-right:25px;margin-top:-8px;">
	                              <label for=""> <small>Actualizar Foto del Perfil</small></label>
	                              <input type="file" name="avatar">
	                           </div>
	                           <input type="hidden" name="_token" value="">
	                           <div class="form-group">
	                              <input type="submit" class="form-control pull-right btn btn-sm btn-primary">
	                           </div>
	                        </form>
						  </div>

						  <div class="col-md-8">
							<form action="" class="pt-md-5">
		                           <div class="form-group">
		                              <input type="text" class="form-control" id="nombres"  placeholder="Nombres" value="{{ Auth::user()->first_name}}">
		                           </div>
		                           <div class="form-group">
		                              <input type="text" class="form-control" id="pwd" placeholder="Apellidos" value="{{ Auth::user()->last_name}}">
		                           </div>
		                           <div class="form-group">
		                              <input type="email" class="form-control" id="pwd" placeholder="Email" value="{{ Auth::user()->email}}">
		                           </div>
		                           <div class="form-group">
		                              <input type="tel" class="form-control" id="pwd" placeholder="Teléfono" value="{{ Auth::user()->phone}}">
		                           </div>
		                           <div class="form-group">
		                              <input type="text" class="form-control" id="pwd" placeholder="Dirección" value="{{ Auth::user()->adress}}">
		                           </div>
		                           <div class="form-group">
			                     	<textarea class="form-control" rows="8" style="max-width: 100%;" id="comment" placeholder="Otros Datos de Interes"></textarea>
			                     </div>
			                     <a href="solicita_clase.php" type="submit" class="form-control pull-right btn btn-sm btn-primary">Actualiza Datos</a>
			                </form>
		                  </div>

	                     </div>
					   </div>
						
					  </div>
		           <div role="tabpanel" class="tab-pane" id="act_contra">
		                  <div class="col-md-12">
		                     <div class="row p-5">
		                        <div class="form-group col-md-12">
		                           <input type="password" class="form-control" placeholder="Nueva Contraseña">
		                        </div>
		                        <div class="form-group col-md-12">
		                           <input type="password" class="form-control" placeholder="Repetir Contraseña">
		                        </div>
		                     </div>
		                  </div>
		                  <div class="row p-5">
		                     <div class="col-md-6 text-left">
		                        <a href="#">¿Olvidaste tu contraseña?</a>
		                     </div>
		                     <div class="col-md-6 text-right">
		                        <button class="btn btn-primary">Guardar Cambios</button>
		                     </div>
		                  </div>
		           </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>
	@endsection
