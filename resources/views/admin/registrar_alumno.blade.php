@extends('layouts.cpanel')
@section('register_alumno')
	<div class="card">
		<div class="card-header">Registrar Perfil</div>
		<div class="card-block">
			<!-- 	<h4 class="text-primary">Datos Personales</h4> -->
			<form class="" action="" method="POST">

			    <div class="form-group row">
					<div class="col-md-12">
				  	  <label for="sel1">Tipo de Perfil:</label>
					  <select class="form-control" id="sel1">
					    <option>Selecciona el perfil</option>
					    <option>Alumno</option>
					    <option>Moderador</option>
					    <option>Profesor</option>
					  </select>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<!-- <label for="email">Nombres:</label> -->
						<div class="form-group">
							<input type="text" class="form-control" id="first_name" placeholder="Nombres y Apellidos">
						</div>
					</div>
					<div class="col-md-6">
						<div class="">
							<!-- <label for="pwd">Email:</label> -->
							<input type="email" class="form-control" id="email" placeholder="Email">
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-group">
							<!-- <label for="pwd">Teléfono:</label> -->
							<input type="tel" class="form-control" id="phone" placeholder="Teléfono">
						</div>
					</div>
					<div class="col-md-6">
						<div class="">
							<!-- <label for="pwd">Dirección:</label> -->
							<input type="text" class="form-control" id="address" placeholder="Dirección">
						</div>
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-group">
							<!-- <label for="pwd">Dirección:</label> -->
							<input type="password" class="form-control" id="password" placeholder="Contraseña">
						</div>
					</div>
					<div class="col-md-6 text-right">
						<button class="btn btn-success" id="btn_save">Registrar</button>
					</div>
				</div>
				
			</form>
		</div>	
	</div>
@endsection