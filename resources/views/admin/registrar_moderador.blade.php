@extends('layouts.cpanel')
@section('register_moderador')
	<div class="card">
		<div class="card-header">Registrar Perfil</div>
		<div class="card-block">
			
			<form class="" action="" method="POST">

			<div class="form-group row">
			<!-- 	<h4 class="text-primary">Datos Personales</h4> -->
				<div class="col-md-12">
				  <label for="sel1">Tipo de Perfil:</label>
				  <select class="form-control" id="sel1">
				    <option>Selecciona el perfil</option>
				    <option>Alumno</option>
				    <option>Moderador</option>
				    <option>Profesor</option>
				  </select>
				</div>
			</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="email">Nombres y Apellidos:</label>
							<input type="text" class="form-control" id="first_name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="pwd">Fecha de Nacimiento:</label>
							<input type="date" class="form-control" id="fecha_nac">
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-6">
					  <div class="form-group">
					  	<select class="form-control" id="sel1">
						    <option>Tipo de Documento</option>
						    <option>DNI</option>
						    <option>CARNET EXT.</option>
						    <option>PASAPORTE</option>
						  </select>
					  </div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<!-- <label for="pwd">Teléfono:</label> -->
							<input type="tel" class="form-control" id="tipo_doc" placeholder="Ejemplo: 70409542">
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="pwd">Teléfono:</label>
							<input type="tel" class="form-control" id="phone">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="pwd">Correo Electronico:</label>
							<input type="email" class="form-control" id="email">
						</div>
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="pwd">Contraseña:</label>
							<input type="password" class="form-control" id="password">
						</div>
					</div>
					<div class="col-md-6">
						<div class="">
							<label for="pwd">Repetir Contraseña:</label>
							<input type="password" class="form-control" id="password">
						</div>
					</div>
				</div>
				<div class="col-md-12 text-right">
					<div class="form-group">
						<button class="btn btn-success" id="btn_save">Registrar</button>
					</div>
				</div>
				
			</form>
		</div>
	</div>
@endsection
</body>