<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
    
    <!-- Datetimepicker css -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
</head>
<body>
    <div id="app">


        @include('partials.navbar')
        
        @yield('content')

        @yield('profile')

        @yield('solicitarclase')

        @yield('detalleclase')


    </div>

    <script>
     window.Laravel = <?php echo json_encode([
     'csrfToken' => csrf_token(),
     ]); ?>
    </script>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
   
     <!-- Jquery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>

    <!-- Script Culqi -->
    <script type="text/javascript" src="https://checkout.culqi.com/v2"></script>

    @yield('scripts')
   
</body>
</html>
