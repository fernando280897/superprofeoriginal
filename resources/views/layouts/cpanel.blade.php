<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'MCDIGITAL') }}</title>

      <!-- tipografia -->
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/style.css') }}" rel="stylesheet">

   </head>
   <body>

      <div id="app"  class="wrapper">

         @component('partials.sidebar')
            @slot('title')
                SuperProfe
            @endslot 
         @endcomponent


         <div id="content" class="container">
    

            @if(Session::has('flash_message'))

            <div class="cont-100">
                <div class="alert alert-success">
                  <a href="" aria-hidden="true" class="close">×</a>
                  <span>{!! session('flash_message') !!}</span>
                </div>
            </div>
            @endif 
            <div class="content">

                @include('partials.navbar-up')
                @yield('register_alumno')
                @yield('register_profesor')
                @yield('register_moderador')
                @yield('solicitar_clase')
            </div>
          
          <!-- end -->
         </div>
        
      </div>
      <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
     </script>
        

     <script src="{{ asset('js/app.js') }}"></script>

     <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>

     <script type="text/javascript">
         $(document).ready(function () {
          // $( "#sidebar" ).hover(
          //     function() {
          //      $('#sidebar').toggleClass('active');
          //     }, function() {
          //       $( this ).removeAttr('class');
          //     }
          //   );
          $('#sidebarCollapse').on('click', function () {
             $('#sidebar').toggleClass('active');
         });
         });
     </script>


   </body>
</html>