@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 p-5"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10 m-auto">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title">Registrar Perfil</h4>
                      </div>
                      <div class="card-block">
                          <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="">
                                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus placeholder="Nombres">

                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="">
                                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Apellidos">

                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                    <div class="">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="E-Mail Address">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">

                                    <div class="">
                                        <input id="tel" type="tel" class="form-control" name="tel" value="{{ old('tel') }}" required placeholder="Teléfono">

                                        @if ($errors->has('tel'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('tel') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">

                                    <div class="">
                                        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required placeholder="Dirección">

                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                    <div class="">
                                        <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">

                                    <div class="">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirmar Contraseña">
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="8" style="max-width: 100%;" id="comment" placeholder="Otros Datos de Interes"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary">
                                            Register
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
