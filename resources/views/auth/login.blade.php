@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 p-5"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 m-auto">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title text-center">SuperProfe</h4>

                      </div>
                    <br>
                    <div class="thumbnail text-center"><img src="{{ asset('images/logo-login.png') }}" class="img-fluid" width="25%" style="    border-radius: 100%;"></div>
          
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                        
                                        <input class="custom-control-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span class="custom-control-indicator"></span>

                                        <span class="custom-control-description">Recordarme</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-outline-primary  ">
                                    Iniciar Sesión
                                </button>
                            </div>
                            <div class="col-md-12 text-center">
                                 <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                            <div class="col-md-12 text-center">
                                <a class="btn btn-primary" href="{{ route('social.auth', 'facebook') }}">
                                    Facebook
                                </a>
                                <a href="{{URL::to('/register')}}" class="btn btn-outline-primary">
                                    Registrate
                                </a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
