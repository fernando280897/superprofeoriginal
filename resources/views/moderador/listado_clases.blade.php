@extends('layouts.app')
   <div class="container-fluid">
      <div class="card">
        <div class="card-header">Listado de Clases</div>
        <div class="card-block">
 <!--          <fieldset class="col-md-12">    -->  
            <!-- <legend >Listado Clases</legend> -->
            <div class="table-responsive row">
                <table class="table table-hover">
                  <thead>
                    <tr class="text-center">
                      <th>Curso / Nivel</th>
                      <th>Centro Educativo</th>
                      <th>Fecha - Hora</th>
                      <th>Adjunto</th>
                      <th>Alumno</th>
                      <th>Asignar Profesor</th>
                      <th>Lugar</th>
                      <th>Estado</th>
                      <th>Observaciones</th>
                      <th>Precio</th>
                      <th>Enviar Factura</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Matemática</td>
                      <td>Sophianum</td>
                      <td>01/10/17 - 10:20 AM</td>
                      <td class="text-center">
                        <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                      </td>
                      <td>Fernando Juarez</td>
                      <td>
                        <a href="#" data-toggle="modal" data-target="#asignar_prof"><i class="fa fa-user-plus"></i>
                        </a>
                        &nbsp; &nbsp;
                        <a href="#"><i class="fa fa-user-times" aria-hidden="true"></i></a>
                      </td>
                      <td>
                        <div class="row">
                          <input class="w-100 form-control" id="ex1" type="text">
                        </div>    
                      </td>
                      <td>Espera</td>
                      <td class="text-center">
                        <a href="#" data-toggle="modal" data-target="#observaciones"><i class="fa fa-comment"></i></a>
                      </td>
                      <td class="row">
                        <div class="input-group">
                          <span class="input-group-addon"><i class=""><b>S/.</b></i></span>
                          <input id="price" type="text" class="w-100 form-control" name="price">
                        </div>
                      </td>
                      <td class="text-center">
                        <a href="#"><span class="fa fa-envelope"></span></a>
                      </td>
                    </tr>
                    <tr>
                      <td>Ingles</td>
                      <td>UPC</td>
                      <td>29/09/17</td>
                      <td class="text-center">
                        <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                      </td>
                      <td>Luis Manuel</td>
                      <td>
                       <a href="#" data-toggle="modal" data-target="#asignar_prof"><i class="fa fa-user-plus"></i>
                       </a>
                       &nbsp; &nbsp;
                       <a href="#"><i class="fa fa-user-times" aria-hidden="true"></i></a>
                     </td>
                     <td>
                      <div class="row">
                        <input class="w-100 form-control " id="ex1" type="text">
                      </div>    
                    </td>
                    <td>Espera</td>
                    <td class="text-center">
                      <a href="#" data-toggle="modal" data-target="#observaciones"><i class="fa fa-comment"></i></a>
                    </td>
                    <td class="row">
                      <div class="input-group">
                        <span class="input-group-addon"><i class=""><b>S/.</b></i></span>
                        <input id="price" type="text" class="w-100 form-control" name="price">
                      </div>   
                    </td>
                    <td class="text-center">
                      <a href="#"><span class="fa fa-envelope"></span></a>
                    </td>
                  </tr>
                  <tr>
                    <td>Matemática</td>
                    <td>Sophianum</td>
                    <td>26/09/17</td>
                    <td class="text-center">
                      <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                    </td>
                    <td>Ricardo Lopez</td>
                    <td>
                     <a href="#" data-toggle="modal" data-target="#asignar_prof"><i class="fa fa-user-plus"></i>
                     </a>
                     &nbsp; &nbsp;
                     <a href="#"><i class="fa fa-user-times" aria-hidden="true"></i></a>
                   </td>
                   <td>
                    <div class="row">
                      <input class="form-control w-100 " id="ex1" type="text">
                    </div>    
                  </td>
                  <td class="success">Atendido</td>
                  <td class="text-center">
                    <a href="#" data-toggle="modal" data-target="#observaciones"><i class="fa fa-comment"></i></a>
                  </td>
                  <td class="row">
                    <div class="input-group">
                      <span class="input-group-addon"><i class=""><b>S/.</b></i></span>
                      <input id="price" type="text" class="w-100 form-control" name="price">
                    </div>
                  </td>
                  <td class="text-center">
                    <a href="#"><span class="fa fa-envelope"></span></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
  <!--       </fieldset> -->
        </div>
      </div>
  </div>
  


<!-- Modal asignar profesor -->

<div class="modal fade" id="asignar_prof"  tabindex="-1" role="dialog" aria-labelledby="asignar_prof" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Asignar Profesor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="col-md-12 p-3">
        <form>
          <div class="input-group form-group">
            <input type="text" class="form-control" placeholder="Ingresar nombre del profesor que desea asignar">
            <div class="input-group-btn">
              <button class="btn btn-default" type="submit">
                <i class="fa fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-12">
        <div class="card">
          <div class="card-block">
            <div class="col-md-12">
              <div class="row">
                <table class="table table-hover">
                <thead class="thead-inverse">
                  <tr>
                    <th>Nombres y Apellidos</th>
                    <th>Curso / Nivel</th>
                    <th>Asignar</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>John</td>
                    <td>Ingles I</td>
                    <td>
                      <div class="">
                        <label><input type="radio" name="optradio"></label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Mary</td>
                    <td>Lenguaje</td>
                    <td>
                      <div class="">
                        <label><input type="radio" name="optradio"></label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>July</td>
                    <td>Matemática</td>
                    <td>
                      <div class="">
                        <label><input type="radio" name="optradio"></label>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 text-right p-3">
        <button class="btn btn-primary">Asignar</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal observaciones -->

  <div class="modal fade" id="observaciones" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Observaciones</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>  
        <div class="modal-body">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos ullam sed aperiam tempore laudantium.</p>
        </div>
         <div class="modal-footer">
            <span>Fernando Juarez Rodriguez</span>
        </div>
      </div>
    </div>
  </div>