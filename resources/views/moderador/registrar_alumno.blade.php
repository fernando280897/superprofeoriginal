@extends('layouts.cpanel')

@section('register_alumno')

<div class="col-md-10 m-a">
	<div class="card">
		<div class="card-header">Registra Alumno</div>
		<div class="card-block">
			<form class="" action="" method="POST">
	
				<div class="col-md-12">
					<div class="form-group">
						<!-- <label for="email">Nombres:</label> -->
						<input type="text" class="form-control" id="first_name" placeholder="Nombres">
					</div>
					<div class="form-group">
						<!-- <label for="pwd">Apellidos:</label> -->
						<input type="text" class="form-control" id="last_name" placeholder="Apellidos">
					</div>
					<div class="form-group">
						<!-- <label for="pwd">Email:</label> -->
						<input type="email" class="form-control" id="email" placeholder="Email">
					</div>
					<div class="form-group">
						<!-- <label for="pwd">Teléfono:</label> -->
						<input type="tel" class="form-control" id="phone" placeholder="Teléfono">
					</div>
					<div class="form-group">
						<!-- <label for="pwd">Dirección:</label> -->
						<input type="text" class="form-control" id="address" placeholder="Dirección">
					</div>
					<div class="form-group">
						<!-- <label for="pwd">Dirección:</label> -->
						<input type="password" class="form-control" id="password" placeholder="Contraseña">
					</div>
	
				</div>
				<div class="col-md-12 text-right">
					<button class="btn btn-success" id="btn_save">Registrar</button>
				</div>
				
			</form>
		</div>
	</div>
</div>

@endsection
