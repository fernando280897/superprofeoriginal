<nav id="sidebar">
    <div class="sidebar-header">
        <h3>{{$title}}</h3>
        <strong>SP</strong>
    </div>

    <ul class="list-unstyled components">
        <li class="active">
            <a id="home" class="bg-white" href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
                <i class="fa fa-home"></i>
                Dashboard
            </a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
                <!-- <li><a href="#" class="bg-white">Home 1</a></li>
                <li><a href="#" class="bg-white">Home 2</a></li>
                <li><a href="#" class="bg-white">Home 3</a></li> -->
            </ul>
        </li>
        <li>
            <!-- <a href="#">
                <i class="glyphicon glyphicon-briefcase"></i>
                About
            </a> -->
            <a id="profiles" href="#pageSubmenu" class="bg-white" data-toggle="collapse" aria-expanded="false">
                <i class="fa fa-users"></i>
                Registrar Perfiles
            </a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
                <li><a href="/registrar-moderador" class="bg-white">Moderador</a></li>
                <li><a href="/registrar-profesor" class="bg-white">Profesor</a></li>
                <li><a href="/registrar-alumno" class="bg-white">Alumno</a></li>
            </ul>
        </li>
        <li>
            <a id="portafolio" href="/solicitar-clase" class="bg-white">
                <i class="fa fa-list-alt"></i>
                Solicitar Clase
            </a>
        </li>
        <!-- <li>
            <a id="faq" href="#" class="bg-white">
                <i class="glyphicon glyphicon-paperclip"></i>
                FAQ
            </a>
        </li>
        <li>
            <a id="" href="#" class="bg-white">
                <i class="glyphicon glyphicon-send"></i>
                Contact
            </a>
        </li> -->
    </ul>

    <ul class="list-unstyled CTAs">
      <!--   <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a></li>
        <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a></li> -->
    </ul>
</nav>

