<div class="container-fluid fixed-top bg-red">
    <nav class="navbar navbar-toggleable-md navbar-light bg-red container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#"><strong>SuperProfe</strong></a>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
          @if (Auth::guest())
<!--           <a class="nav-item nav-link" href="#">Login</a> -->
<!--           <a class="nav-item nav-link" href="#">Registrate</a> -->
          @else
          <div class="btn-group">
              <a href="#" class="dropdown-toggle a-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img class="img-avatar rounded-circle" src="{{ Auth::user()->avatar }}" style="">
               {{ Auth::user()->first_name }} <span class="caret"></span>
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item " href="{{ route('register') }}">Mi Perfil</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Cerrar Sesión
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </div>
          </div>
           @endif
        </div>
      </div>
    </nav>
</div>