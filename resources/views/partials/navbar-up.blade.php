<nav class="navbar navbar-default">
    <div class="">

        <div class="navbar-header">
            <button type="button" id="sidebarCollapse" class="btn btn-danger navbar-btn">
                <i class="glyphicon glyphicon-align-left"></i>
                <span class="fa fa-bars"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
            </ul>
        </div>
    </div>
</nav>