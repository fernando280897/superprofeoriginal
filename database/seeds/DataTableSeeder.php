<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ask for confirmation to refresh migration
        if ($this->command->confirm('¿Desea actualizar la migración antes del seeds, Asegúrese de que borrará todos los datos antiguos?')) {
            $this->command->call('migrate:refresh');
            $this->command->warn("Se borraron los datos, a partir de la nueva base de datos.");
        }

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $permission) {
            Permission::firstOrCreate(['name' => $permission]);
        }

        $this->command->info('Permisos predeterminados añadidos.');

        // Ask to confirm to assign admin or user role
        if ($this->command->confirm('Crear roles para el usuario, por defecto es admin y usuario? [y | N]', true)) {

            // Ask for roles from input
            $roles = $this->command->ask('Introduzca los roles en formato separado por comas.', 'Admin, Moderador, Profesor, Alumno');

            // Explode roles
            $rolesArray = explode(',', $roles);

            // add roles
            foreach($rolesArray as $role) {
                $role = Role::firstOrCreate(['name' => trim($role)]);

                // if( $role->name == 'Admin' ) {
                //     // assign all permissions to admin role
                //     $role->permissions()->sync(Permission::all());
                //     $this->command->info('Admin tendrá todos los derechos');
                // } else {
                //     // for others, give access to view only
                //     $role->permissions()->sync(Permission::where('name', 'LIKE', 'view_%')->get());
                // }

                switch ($role) {
				    case 'Admin':
				        $role->permissions()->sync(Permission::all());
                    	$this->command->info('Admin tendrá todos los derechos');
				        break;
				    case 'Moderador':
				        echo "Moderador aun no tiene permisos";
				        break;
				    case 'Profesor':
				        echo "Profesor aun no tiene permisos";
				        break;
				    case 'Alumno':
				        echo "Alumno aun no tiene permisos";
				        break;
				}

                // create one user for each role
                // $this->createUser($role);
            }

            $this->command->info('Roles ' . $roles . ' añadidos correctamente.');

        } else {
            Role::firstOrCreate(['name' => 'Moderador']);
            $this->command->info('De forma predeterminada, se agregó el rol de moderador.');
        }

    }

     /**
     * Create a user with given role
     *
     * @param $role
     */
    // private function createUser($role)
    // {
    //     $user = factory(User::class)->create();
    //     $user->assignRole($role->name);

    //     if( $role->name == 'Admin' ) {
    //         $this->command->info('Admin login details:');
    //         $this->command->warn('Username : '.$user->email);
    //         $this->command->warn('Password : "secret"');
    //     }
    // }
}
