<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

		for($i = 0; $i < 30; $i ++)

		{

		$id = \DB::table('persons')->insertGetId(array(

		    'firstname' =>  $faker->firstName,
	        'lastname' => $faker->lastName,
	        'telephone' => $faker->e164PhoneNumber,
	        'address' => $faker->address,
	        'email' => $faker->unique()->email,
	        // 'remember_token' => str_random(10)

		));

		\DB::table('users')->insert(array(

		   'persons_id' => $id,
		   'user_name' => $faker->unique()->userName, 
		   'password' => \Hash::make('12345'),
		   // 'remember_token' => str_random(10)
		));


		}
    }
}
