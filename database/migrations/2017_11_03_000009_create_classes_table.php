<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'classes';

    /**
     * Run the migrations.
     * @table classes
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            // $table->increments('id');
            $table->integer('states_id')->unsigned();
            $table->integer('pay_states_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->integer('courses_id')->unsigned();
            $table->integer('levels_id')->unsigned();
            $table->string('institution');
            $table->date('date');
            $table->time('hour');
            $table->string('description')->nullable();
            $table->string('homework')->nullable();
            $table->timestamps();

            $table->primary(['users_id', 'date', 'hour']);


            $table->foreign('states_id')->references('id')->on('states')->onDelete('cascade');

            $table->foreign('pay_states_id')->references('id')->on('pay_states')->onDelete('cascade');

        
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('courses_id')->references('id')->on('courses')->onDelete('cascade');

            $table->foreign('levels_id')->references('id')->on('levels')->onDelete('cascade');

            // end
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
