<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'polls';

    /**
     * Run the migrations.
     * @table polls
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            // $table->increments('id');
            $table->integer('classes_id')->unsigned();
            $table->integer('qualities_id')->unsigned();
            $table->string('answer');
            $table->timestamps();

            $table->foreign('qualities_id')->references('id')->on('qualities')->onDelete('cascade');

            $table->foreign('classes_id')->references('id')->on('classes')->onDelete('cascade');

            // end
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
