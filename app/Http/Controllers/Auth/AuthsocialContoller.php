<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use Socialite;
use Carbon\Carbon;


class AuthsocialContoller extends Controller
{
     // Metodo encargado de la redireccion a Facebook
    public function redirectToProvider($provider)
    {
        $scopes = [
            'public_profile',
            'email'
        ];
        return Socialite::driver($provider)->scopes($scopes)->redirect();
        //return Socialite::with('facebook')->redirect()->getTargetUrl();
        
    }
    
    // Metodo encargado de obtener la información del usuario
    public function handleProviderCallback($provider)
    {

        //Obtenemos los datos del usuaro
        $social_user = Socialite::driver($provider)->stateless()->user();
        //Comprobamos si el usuario ya existe
        if ($user = User::where('email', $social_user->email)->first()) { 
            return $this->authAndRedirect($user); // Login y redirección
        } else {  
            // En caso de que no exista creamos un nuevo usuario con sus datos.
            if(isset($social_user->user['birthday']))
            {
                $btd=date('Y-m-d', strtotime($social_user->user['birthday']));
            } else{
                $btd=null;
            }
        }
            $user = User::create([
                'first_name' => $social_user->user['first_name'],
                'last_name' => $social_user->user['last_name'],
                'birthday' => $btd,
                'gender' => $this->genero($social_user->user['gender']),
                'email' => $social_user->user['email'],
                'avatar' => $social_user->avatar
            ]);
            return $this->authAndRedirect($user); // Login y redirección
            
        }

    // Login y redirección
    public function authAndRedirect($user)
    {
        Auth::login($user);
 
        return redirect()->to('/perfil#');
    }

    public function genero($genero){

        switch ($genero) {
            case 'male':
                $res = 'Masculino';
                break;
            case 'female':
                $res = 'Femenino';
                break;
            default:
               $res = '';
                break;
        }
        return $res;
    }
}
