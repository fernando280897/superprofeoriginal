<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
   protected $table = 'persons';
    // protected $primaryKey = 'contact_id';

    public $timestamps=false;

	//campos que van a recibir un valor para almacenarlo en la base de datos
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable=[

	    'first_name',
	    'last_name',
	    'telephone',
	    'address',
	    'email'
    ];

    public function getFullNameAttribute()
    {   
        return $this->first_name. ' ' . $this->last_name;
    }

    public function user()
	{
	  return $this->belongsTo(User::class);
	}


}
