<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Class extends Model
{
    protected $table = 'classes';
    // protected $primaryKey = 'contact_id';

    public $timestamps=false;

	//campos que van a recibir un valor para almacenarlo en la base de datos
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable=[

	    'states_id',
	    'pay_states_id',
	    'user_id',
	    'courses_id',
	    'levels_id',
	    'institucion',
	    'date',
	    'hour',
	    'description',
	    'homework'
    ];

}
