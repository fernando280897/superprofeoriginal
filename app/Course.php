<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    // protected $primaryKey = 'contact_id';

    public $timestamps=false;

	//campos que van a recibir un valor para almacenarlo en la base de datos
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable=[

	    'name'
    ];
}
