<?php

//PRINCIPAL

Route::get('/', function () {
    return view('welcome');
});


// RUTAS ALUMNO

Route::get('auth/{provider}', 'Auth\AuthsocialContoller@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\AuthsocialContoller@handleProviderCallback');
Route::resource('perfil', 'UserController');
Route::post('perfil', 'UserController@update_avatar');
Route::get('fecha', 'fechaController@getFecha');
Route::resource('solicitar-clase', 'Alumno\SolicitarclaseController');
Route::get('detalle-clase', 'Alumno\SolicitarclaseController@detalleclase');



// RUTAS PROFESOR

// Route::resource('listado-clase', 'Profesor\ListarClaseController');
// Route::resource('detalle-pedido', 'Profesor\DetallePedidoController');


// RUTAS MODERADOR

// Route::resource('registrar-alumno', 'Moderador\RegisterAlumnoController');
// Route::resource('solicitar-clase', 'Moderador\SolicitarClaseController');
// Route::resource('listado-clase', 'Moderador\ListarClaseController');
// Route::resource('detalle-pago', 'Moderador\DetallePagoController');


//RUTAS ADMINISTRADOR


// Route::resource('registrar-moderador', 'Admin\RegisterModeradorController');
// Route::resource('registrar-profesor', 'Admin\RegisterProfesorController');
// Route::resource('registrar-alumno', 'Admin\RegisterAlumnoController');

Auth::routes();
